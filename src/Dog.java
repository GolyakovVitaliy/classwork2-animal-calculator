public class Dog extends Animal{
    private int x;
    private String s;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    @Override
    protected void run() {
        System.out.println("бежит");
    }

    @Override
    protected void golos() {
        System.out.println("гавкает");
    }
}

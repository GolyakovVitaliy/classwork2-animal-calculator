import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame{
    public Frame() throws HeadlessException {
        setBounds(600,200,225,310);
        setResizable(false);
        setTitle("Calculator");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(new Panel());
        setVisible(true);
    }

    public class Panel extends JPanel{
        private JTextField txt;
        private JTextField txt2;
        private JButton bt1;
        private JButton bt2;
        private JButton bt3;
        private JButton bt4;
        private JButton bt5;
        private JButton bt6;
        private JButton bt7;
        private JButton bt8;
        private JButton bt9;
        private JButton bt0;
        private JButton bt_c;
        private JButton bt_del;
        private JButton bt_um;
        private JButton bt_plus;
        private JButton bt_minus;
        private JButton bt_res;
        private double firstvalue=0;
        private char operation=' ';

        Panel() {

            setLayout(null);
            setBackground(Color.GRAY);
            init();
            add (txt);
            add (txt2);
            add (bt1);
            add (bt2);
            add (bt3);
            add (bt4);
            add (bt5);
            add (bt6);
            add (bt7);
            add (bt8);
            add (bt9);
            add (bt0);
            add(bt_c);
            add(bt_del);
            add(bt_um);
            add(bt_plus);
            add(bt_minus);
            add(bt_res);
            listener();

        }
private void init() {
            txt = new JTextField();
            txt.setBounds(10,10,200,50);
            txt.setFont(new Font("Anonimous", Font.BOLD, 25));

    txt2 = new JTextField();
    txt2.setBounds(60,260,100,15);
    txt2.setFont(new Font("Anonimous", Font.PLAIN, 10));
    txt2.setText("    КАЛЬКУЛЯТОР");
    txt2.setEnabled(false);

            bt1 = new JButton("1");
            bt1.setBounds(10,60,50,50);

    bt2 = new JButton("2");
    bt2.setBounds(60,60,50,50);

    bt3 = new JButton("3");
    bt3.setBounds(110,60,50,50);

    bt4 = new JButton("4");
    bt4.setBounds(160,60,50,50);

    bt5 = new JButton("5");
    bt5.setBounds(10,110,50,50);

    bt6 = new JButton("6");
    bt6.setBounds(60,110,50,50);

    bt7 = new JButton("7");
    bt7.setBounds(110,110,50,50);

    bt8 = new JButton("8");
    bt8.setBounds(160,110,50,50);

    bt9 = new JButton("9");
    bt9.setBounds(10,160,50,50);

    bt0 = new JButton("0");
    bt0.setBounds(60,160,50,50);

    bt_res = new JButton("=");
    bt_res.setBounds(110,160,50,50);

    bt_c = new JButton("C");
    bt_c.setBounds(160,160,50,50);

    bt_plus = new JButton("+");
    bt_plus.setBounds(10,210,50,50);

    bt_minus = new JButton("-");
    bt_minus.setBounds(60,210,50,50);

    bt_um = new JButton("*");
    bt_um.setBounds(110,210,50,50);

    bt_del = new JButton("/");
    bt_del.setBounds(160,210,50,50);


}

private void listener() {
            bt1.addActionListener(e-> txt.setText(txt.getText()+1));
    bt2.addActionListener(e-> txt.setText(txt.getText()+2));
    bt3.addActionListener(e-> txt.setText(txt.getText()+3));
    bt4.addActionListener(e-> txt.setText(txt.getText()+4));
    bt5.addActionListener(e-> txt.setText(txt.getText()+5));
    bt6.addActionListener(e-> txt.setText(txt.getText()+6));
    bt7.addActionListener(e-> txt.setText(txt.getText()+7));
    bt8.addActionListener(e-> txt.setText(txt.getText()+8));
    bt9.addActionListener(e-> txt.setText(txt.getText()+9));
    bt0.addActionListener(e-> txt.setText(txt.getText()+0));

    bt_c.addActionListener(e->{
        String temp = txt.getText();
        txt.setText(temp.substring(0, temp.length()-1));

    });

    bt_um.addActionListener(e->{
        firstvalue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '*';
    });
    bt_del.addActionListener(e->{
        firstvalue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '/';
    });
    bt_plus.addActionListener(e->{
        firstvalue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '+';
    });
    bt_minus.addActionListener(e->{
        firstvalue = Double.valueOf(txt.getText());
        txt.setText("");
        operation = '-';
    });
    bt_res.addActionListener(e->{
        double secondvalue = Integer.valueOf(txt.getText());
        switch (operation){
            case '+': txt.setText(String.valueOf(firstvalue+secondvalue));
            break;
            case '-': txt.setText(String.valueOf(firstvalue-secondvalue));
                break;
            case '*': txt.setText(String.valueOf(firstvalue*secondvalue));
                break;
            case '/': txt.setText(String.valueOf(firstvalue/secondvalue));
                break;
        }
    });
}


    }
}


